import * as constraintsMap from './constraints';
import { isDefined } from '../../library/value';

/**
 * Calls a validation function (if it exists) with the same name for every entry
 * in the constraints array, with a value and optional configuration parameters.
 * 
 * If no value is provided, or if the value does not pass validation, return the
 * name of the constraint.
 * 
 * @param {*} value
 * @param {Array} constraints Array of strings, i.e. ['required', 'email']
 * @param {Object} config optional additional parameters
 * @return {string|undefined}
 */
const validate = (value, constraints = [], config) => {
  const filteredConstraints = constraints.filter(c => constraintsMap[c]);

  const valid = constraint =>
    isDefined(value) && constraintsMap[constraint](value, config);

  for (const constraint of filteredConstraints) {
    if (!valid(constraint)) {
      return constraint;
    }
  }
};

export default validate;
