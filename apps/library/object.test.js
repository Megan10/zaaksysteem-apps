import { extract, purge } from './object';

const { assign } = Object;

describe('The `object` module', () => {
  const sourceObject = {
    foo: 'FOO',
    bar: 'BAR',
    quux: 'QUUX',
  };

  /**
   * @test {extract}
   */
  describe('exports an `extract` function that', () => {
    test('can extract a single property with the key as first argument', () => {
      const actual = extract('bar', sourceObject);
      const expected = ['BAR', {
        foo: 'FOO',
        quux: 'QUUX',
      }];

      expect(actual).toEqual(expected);
    });

    test('can extract a single property with the key as last argument', () => {
      const actual = extract(sourceObject, 'bar');
      const expected = [{
        foo: 'FOO',
        quux: 'QUUX',
      }, 'BAR'];

      expect(actual).toEqual(expected);
    });

    test('can extract multiple properties in any argument order', () => {
      const actual = extract('quux', sourceObject, 'foo');
      const expected = ['QUUX', {
        bar: 'BAR',
      }, 'FOO'];

      expect(actual).toEqual(expected);
    });

    test('throws if no argument is an object', () => {
      function actual() {
        extract('foo', 'bar');
      }

      expect(actual).toThrow();
    });

    test('throws if more than one argument is an object', () => {
      function actual() {
        extract('quux', sourceObject, 'foo', sourceObject);
      }

      expect(actual).toThrow();
    });
  });

  /**
   * @test {purge}
   */
  describe('exports a `purge` function that', () => {
    test('accepts its arguments in any order and returns an object without the supplied properties', () => {
      const clone = assign({}, sourceObject);
      const actual = purge('foo', clone, 'quux');
      const expected = {
        bar: 'BAR',
      };

      expect(actual).toEqual(expected);
    });
  });
});
