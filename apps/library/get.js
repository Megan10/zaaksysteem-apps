// ZS-FIXME: complicated and unreadable.
// 1) add ESDoc documentation for function purpose, paramaters and return value
// 2) write unit tests
// 3) enable ESLint
// 4) refactor
/*eslint-disable*/
function get(obj, path, def) {
  let fullPath = path
    .replace(/\[/g, '.')
    .replace(/]/g, '')
    .split('.')
    .filter(Boolean);

  if (!obj) return def || undefined;

  return fullPath.every(everyFunc) ? obj : def;

  function everyFunc(step) {
    return !(step && obj && (obj = obj[step]) === undefined);
  }
}

export default get;
