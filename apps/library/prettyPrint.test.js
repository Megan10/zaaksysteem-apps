import { multiLine } from './prettyPrint';

describe('The `prettyPrint` module', () => {
  describe('exports a multiLine function that', () => {
    test('joins multiple strings with a space for source code readability', () => {
      const actual = multiLine(
        'a',
        'b',
        'c'
      );
      const expected = multiLine('a b c');

      expect(actual).toBe(expected);
    })
  });
});
