import { runQueue } from './runQueue';

/**
 * @test {runQueue}
 */
describe('The `runQueue` module', () => {
  let a;
  let b;
  let c;

  beforeEach(() => {
    a = jest.fn();
    b = jest.fn();
    c = jest.fn();
  });

  test('executes an array of functions', () => {
    runQueue([a, b, c]);
    expect(a).toHaveBeenCalled();
    expect(b).toHaveBeenCalled();
    expect(c).toHaveBeenCalled();
  });

  test('executes an array of functions with a givven parameter', () => {
    runQueue([a, b, c], 'foo');
    expect(a).toHaveBeenCalledWith('foo');
    expect(b).toHaveBeenCalledWith('foo');
    expect(c).toHaveBeenCalledWith('foo');
  });

  test('executes an array of functions with an array of parameters', () => {
    runQueue([a, b, c], ['foo', 'bar']);
    expect(a).toHaveBeenCalledWith('foo', 'bar');
    expect(b).toHaveBeenCalledWith('foo', 'bar');
    expect(c).toHaveBeenCalledWith('foo', 'bar');
  });
});
