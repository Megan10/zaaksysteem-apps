/**
 * Redux reducer function factory.
 *
 * @param {*} initialState
 *   Initial store state.
 * @param {Object} reducers
 *   Object that associates action types with state reducer functions.
 */
export const createReducer = (initialState, reducers) =>
  (state = initialState, action) => {
    const { payload, type } = action;

    if (typeof reducers[type] === 'function') {
      return reducers[type]({ payload, state });
    }

    return state;
  };
