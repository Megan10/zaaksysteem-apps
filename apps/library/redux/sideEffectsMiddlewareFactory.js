import { asArray } from '../../library/array';

/**
 * Factory function for creating a simple side effetcs middleware for redux.
 *
 * @param {Object} sideEffects
 *   An object with property names that correspond to action types.
 * @return {Function}
 */
/*eslint-disable */
export const reduxSideEffectsMiddlewareFactory = sideEffects =>
  store =>
    next =>
      action => {
        const { payload, type } = action;

        if (sideEffects.hasOwnProperty(type)) {
          const { dispatch } = store;

          sideEffects[type].forEach(sideEffect => {
            sideEffect(payload, store)
              .then(function (actions) {
                const actionList = asArray(actions);

                actionList.forEach(action => action && dispatch(action()));
              });
          });
        }

        return next(action);
      };
