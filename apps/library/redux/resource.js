export const TYPE_ERROR = 'Fatal: Bad resource descriptor';

const { isArray } = Array;

const isObject = value =>
  typeof value == 'object'
  && value !== null
  && !isArray(value);

const isData = value =>
  isObject(value);

const isId = value =>
  typeof value == 'string';

function isOrderedPairTuple(parameters) {
  const [id, data] = parameters;

  return isId(id) && isData(data);
}

const isIdOrOrderedPairTuple = value =>
  isId(value)
  || isOrderedPairTuple(value);

const isIdOrOrderedPairTupleSet = parameters =>
  isArray(parameters)
  && parameters
    .every(isIdOrOrderedPairTuple);

function mapIdOrOrderedPairTupleSet(item) {
  if (isId(item)) {
    return [item, null];
  }
  return item;
}

const isCustom = resourceDescriptor => {
  const [descriptor] = resourceDescriptor;
  return (descriptor && typeof descriptor === 'object');
};

/* eslint complexity: [2, 10] */
export function normalize(resourceDescriptor) {

  if (isId(resourceDescriptor)) {
    return [[resourceDescriptor, null]];
  }

  if (isOrderedPairTuple(resourceDescriptor) || isCustom(resourceDescriptor)) {
    return [resourceDescriptor];
  }

  if (isIdOrOrderedPairTupleSet(resourceDescriptor)) {
    return resourceDescriptor
      .map(mapIdOrOrderedPairTupleSet);
  }

  throw new TypeError(TYPE_ERROR);
}
