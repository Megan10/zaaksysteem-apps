import callOrNothingAtAll from './callOrNothingAtAll';

const HELLO = 'Hello, world!';
const OR_NOTHING_AT_ALL = 42;
const ONE = 1;
const call = jest.fn(() => HELLO);

test('callOrNothingAtAll', () => {
  const callResult = callOrNothingAtAll(call);
  const orNothingAtAllResult = callOrNothingAtAll(OR_NOTHING_AT_ALL);

  expect(callResult).toBe(HELLO);
  expect(orNothingAtAllResult).toBe(undefined);
  expect(call.mock.calls.length).toBe(ONE);
});
