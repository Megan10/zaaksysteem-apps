import get from '../get';

const { assign } = Object;

/**
 * Returns an array of all form items that have been changed
 * (form values are different than those in the items resource)
 *
 * @param {Object} form
 * @param {Array} items
 * @return {Array}
 */
const fieldsChanged = (form, items) =>
  Object.entries(form).reduce((accumulator, entry) => {
    
    const [name, value] = entry;
    const resourceItem = items.find(i => i.name === name);
    const originalValue = get(resourceItem, 'value');
    const isNew = () => !resourceItem;
    const isExisting = () =>
      resourceItem &&
      JSON.stringify(originalValue) !== JSON.stringify(value);

    if (isNew()) {
      accumulator.push(assign({}, resourceItem, {
        value,
      }));
    }

    if (isExisting()) {
      accumulator.push(assign({}, resourceItem, {
        value,
        originalValue,
      }));
    }

    return accumulator;
  }, []);

export {
  fieldsChanged,
};
