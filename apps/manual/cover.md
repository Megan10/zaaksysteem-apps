# Zaaksysteem Single Page Application (SPA) Repository

> New *Zaaksysteem Apps* are hosted in a dedicated VCS repository. See 
  [working copy dependencies](./manual/docker.html#vcs-working-copy-dependencies)
  for context requirements.

## High level project structure

- [**apps**](./manual/app.html)
    - If you are a front-end developer, that's the only 
      directory you care about.
- dev-bin
    - shell scripts
- Docker
    - Docker assets
- node
    - [**build system**](./manual/build-system.html)
    - `bash` initialization for 
      [**development**](./manual/development.html) in the Docker container
- npm
    - The `package(-lock)?.json` files under version control.
      **Do not edit them manually.**
