# — Zaaksysteem *Beheerder* SPA

## Technology stack

* [React >= 16]()
* [Redux]()

## Tiers

Every tier must be replaceable with minimal impact.

- **View**
    - *Presentational Components* render the *graphical user interface*.
    - *Container Components* connect *Presentational Components* to the **Store**.
        - Introduction: [Presentational and Container Components](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0)
        - *Container Components* are contaminated by the **Store** API.
- **Store**
    - The **Store** manages the *application state* and passes new data to the connected **View** components.
    - Store actions can trigger *Side Effects*, in particular resolving **Resource**s.
        - Resource *Side Effects* are contaminated by the **Resource** API.
- **Resource**
    - The **Resource** tier resolves all data, e.g. fixtures and network requests.
    - The **Resource** tier is and must remain pure.
