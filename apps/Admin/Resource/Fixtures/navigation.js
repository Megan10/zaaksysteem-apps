/**
 * Array of navigation link configurations with the signature
 * - `{string}` icon
 * - `{string}` label
 * - `{string}` path
 *
 * @type {Array<Object>}
 */
export const navigation = [
  {
    icon: 'chrome_reader_mode',
    label: 'Catalogus',
    path: '/admin/catalogus',
  },
  {
    icon: 'people',
    label: 'Gebruikers',
    path: '/admin/gebruikers',
  },
  {
    icon: 'import_contacts',
    label: 'Logboek',
    path: '/admin/logboek',
  },
  {
    icon: 'poll',
    label: 'Transactieoverzicht',
    path: '/admin/transactieoverzicht',
  },
  {
    icon: 'all_inclusive',
    label: 'Koppelingsconfiguratie',
    path: '/admin/koppelingsconfiguratie',
  },
  {
    icon: 'view_comfy',
    label: 'Gegevensmagazijn',
    path: '/admin/gegevensmagazijn',
  },
  {
    icon: 'settings',
    label: 'Configuratie',
    path: '/admin/configuratie',
  },
];
