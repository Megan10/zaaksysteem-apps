# Fixtures

> Fixtures can be used to retrieve static data the same
  way as API transactions, i.e. resolve a *resource* by *id*.

## Requirements

- data must be JSON, so fixture data can easily be moved to
  an API endpoint

## Use cases

- configuration data
- mocks for API endpoints 
  (takes precedence over `../API/map`)
