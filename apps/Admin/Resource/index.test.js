import resource from '.';

const ONE = 1;

/**
 * @test {resource}
 */
describe('The `resource` module', () => {
  test('rejects the promise if the resource ID is unknown', () => {
    expect.assertions(ONE);

    resource('nil')
      .catch(reason =>
        expect(reason.message)
          .toBe('Resource: Fatal: Unknown resource ID or map "nil"'));
  });
});
