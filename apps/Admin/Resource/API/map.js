import dictionary from '../../../library/dictionary';

/**
 * Resource ID dictionary for Zaaksysteem API requests.
 *
 * @type {Object}
 */
export const map = dictionary({
  config: {
    method: 'GET',
    url: '/api/v1/config/panel',
  },
  session: {
    method: 'GET',
    url: '/api/v1/session/current',
  },
  'config:save': {
    method: 'POST',
    url: '/api/v1/config/update?rows_per_page=200',
  },
});
