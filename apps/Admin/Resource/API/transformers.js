/*
───────────▄▄▄▄▄▄▄▄▄───────────
────────▄█████████████▄────────
█████──█████████████████──█████
▐████▌─▀███▄───────▄███▀─▐████▌
─█████▄──▀███▄───▄███▀──▄█████─
─▐██▀███▄──▀███▄███▀──▄███▀██▌─
──███▄▀███▄──▀███▀──▄███▀▄███──
──▐█▄▀█▄▀███─▄─▀─▄─███▀▄█▀▄█▌──
───███▄▀█▄██─██▄██─██▄█▀▄███───
────▀███▄▀██─█████─██▀▄███▀────
───█▄─▀█████─█████─█████▀─▄█───
───███────────███────────███───
───███▄────▄█─███─█▄────▄███───
───█████─▄███─███─███▄─█████───
───█████─████─███─████─█████───
───█████─████─███─████─█████───
───█████─████─███─████─█████───
───█████─████▄▄▄▄▄████─█████───
────▀███─█████████████─███▀────
──────▀█─███─▄▄▄▄▄─███─█▀──────
─────────▀█▌▐█████▌▐█▀─────────
────────────███████────────────
*/

import get from '../../../library/get';
import { asArray, isPopulatedArray } from '../../../library/array';
import i18n from '../../../library/i18next';
import {
  isEmpty,
  isArrayOfStrings,
  isArrayOfDbObjects,
  isDbObject,
  isSelect,
  isArrayOfSelects,
  selectToDb,
  arrayToDb,
  objectToSelect,
  arrayToSelect,
} from './library/transformers';
import { traverseMap } from '../../../library/map';

/**
 * @param {string} a
 * @param {string} b
 * @return {number}
 */
const sortByLabel = (a, b) =>
  a.label.localeCompare(b.label);

/**
 * @param {Object} parameters
 * @param {string} parameters.type
 * @param {boolean} parameters.required
 * @return {Array}
 */
const getConstraints = ({ type, required }) => {
  const constraints = [];

  if (['email', 'uri', 'number'].includes(type)) {
    constraints.push(type);
  }
  if (required) {
    constraints.push('required');
  }

  return constraints;
};

/**
 * @param {Object} parameters
 * @param {string} parameters.type
 * @param {string} parameters.name
 * @param {Object} parameters.config
 * @return {string}
 */
const getComponentType = ({ type, name, config }) => {
  function getFormat() {
    return get(config, 'format') === 'html' ? 'html' : 'text';
  }
  
  const map = new Map([
    [name => name === 'custom_relation_roles', () => 'creatable'],
    [name => name === 'allowed_templates', () => 'select'],
    [(name, type) => type === 'boolean', () => 'checkbox'],
    [(name, type) => type === 'text', () => getFormat()],
    [(name, type) => type === 'object_ref', () => 'select'],
  ]);

  return traverseMap({
    map,
    keyArgs: [name, type],
    functionArgs: [name, type],
    fallback: 'text',
  });
};

/**
 * @param {string} type
 * @return {Object|undefined}
 */
const getTranslations = type => {
  if (['select', 'creatable'].includes(type)) {
    return ['loading', 'choose', 'beginTyping', 'creatable', 'create'].reduce((accumulator, entry) => {
      accumulator[`form:${entry}`] = i18n.t(`form:${entry}`);
      return accumulator;
    }, {});
  }
};

/**
 * @param {*} value_type
 * @return {Object|undefined}
 */
const getConfig = value_type =>
  get(value_type, 'options');

/**
 * @param {string} name
 * @return {Object|undefined}
 */
const getChoicesParameters = name => {
  const emailTemplate = {
    url: '/api/v1/email_template',
    match: 'label',
    name,
  };
  const group = {
    url: '/api/v1/group',
    match: 'description',
    name,
  };
  const role = {
    url: '/api/v1/role',
    match: 'description',
    name,
  };
  const municipalities = {
    url: '/api/v1/general/municipality_code',
    match: 'name',
    name,
    rows: 50,
  };

  const map = {
    'allocation_notification_template_id': emailTemplate,
    'feedback_email_template_id': emailTemplate,
    'new_user_template': emailTemplate,
    'subject_pip_authorization_confirmation_template_id': emailTemplate,
    'case_distributor_group': group,
    'case_distributor_role': role,
    'signature_upload_role': role,
    'bag_priority_gemeentes': municipalities,
  };

  if (map[name]) {
    return map[name];
  }
};

/**
 * @param {*} name
 * @return {boolean}
 */
const getAutoLoad = name =>
  ['case_distributor_group', 'case_distributor_role', 'signature_upload_role'].includes(name);

/**
 * Transforms field value(s) from app state values to valid backend values
 * @param {*} value
 * @return {*}
 */
const appToDb = value => {
  const map = new Map([
    [value => isEmpty(value), () => null],
    [value => isSelect(value), value => selectToDb(value)],
    [value => isArrayOfSelects(value), value => value.map(thisValue => selectToDb(thisValue))],
    [value => isArrayOfStrings(value), value => arrayToDb(value)],
  ]);

  return traverseMap({
    map,
    keyArgs: [value],
    functionArgs: [value],
    fallback: value,
  });
};

const getChoices = value_type =>
  get(value_type, 'choices');

/**
 * Transform field value(s) from database to valid app state values
 * @param {*} value
 * @return {*}
 */
const dbToApp = value => {
  const map = new Map([
    [value => isDbObject(value), value => objectToSelect(value)],
    [value => isArrayOfStrings(value), value => arrayToSelect(value)],
    [value => isArrayOfDbObjects(get(value, 'instance.rows')), value => asArray(get(value, 'instance.rows')).map(row => objectToSelect(row))],
  ]);

  return traverseMap({
    map,
    keyArgs: [value],
    functionArgs: [value],
    fallback: value,
  });
};

/**
 * @type {Array}
 */
const transformers = [
  {
    match: [
      '^/api/v1/session',
    ],
    transform: response => get(response, 'result.instance'),
  },
  {
    match: [
      '^/api/v1/config/update',
    ],
    transform(response) {
      const rows = get(response, 'result.instance.rows');

      const map = ({
        reference,
        instance: {
          name,
        },
      }) => ({
        name,
        reference,
      });

      return asArray(rows).map(map);
    },
  },
  {
    match: [
      '^/api/v1/(group|role|email_template|general/municipality_code)',
    ],
    transform(response) {
      const rows = get(response, 'result.instance.rows');
      return asArray(rows)
        .map(row => objectToSelect(row))
        .sort(sortByLabel);
    },
  },
  {
    match: [
      '^/api/v1/config/panel',
    ],
    transform(response) {
      const {
        result: {
          instance: {
            definitions: { instance: { rows: definitionsRows } },
            items: { instance: { rows: itemsRows } },
          },
        },
      } = response;

      const createItemDefinition = ({
        reference: definitionReference,
        instance: {
          config_item_name: name,
          label,
          value_type,
          value_type_name,
          mvp: isMulti,
          mutable,
          required,
        },
      }) => {

        const item = itemsRows.find(({ instance: { definition: { reference: thisReference } } }) => thisReference === definitionReference);
        const reference = get(item, 'reference');
        const type = (value_type_name) ? value_type_name : (get(value_type, 'parent_type_name') || get(value_type, 'name'));
        const value = dbToApp(get(item, 'instance.value'));
        const config = getConfig(value_type);
        const constraints = getConstraints({
          type,
          required,
        });
        const componentType = getComponentType({
          type,
          name,
          config,
        });
        const translations = getTranslations(componentType);
        const choicesParameters = getChoicesParameters(name);
        const autoLoad = getAutoLoad(name);
        const choices = dbToApp(getChoices(config));
        const hasInitialChoices = isPopulatedArray(choices);

        /* ZS-FIXME: */
        /* eslint complexity: [2, 8] */

        return {
          type: componentType,
          config,
          label,
          name,
          isMulti,
          disabled: !mutable,
          reference,
          required,
          hasInitialChoices,
          value,
          ...(constraints && { constraints }),
          ...(choicesParameters && { choicesParameters }),
          ...(autoLoad && { autoLoad }),
          ...(translations && { translations }),
          ...(choices && { choices }),
        };
      };

      return {
        items: definitionsRows.map(createItemDefinition).filter(i => i.reference),
      };
    },
  },
];

export {
  transformers,
  appToDb,
};
