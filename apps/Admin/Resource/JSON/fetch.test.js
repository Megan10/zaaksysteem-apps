/**
 * Cf. the JSDOM `fetch` stub in /client.next/test/jest-setup
 */
import { request } from './fetch';

const RESOURCE_ID = 'asdf';
const ONE = 1;
const STATUS_OK = 200;
const ANSWER = 42;

const getResponseStub = instance => ({
  result: {
    instance: {
      pager: {},
      rows: [
        {
          instance,
        },
      ],
    },
  },
  status_code: STATUS_OK,
});

const isPromise = value => (value instanceof Promise);

/**
 * @test {request}
 */
describe('The `json` module', () => {

  beforeEach(() => {
    window.fetch.stub(getResponseStub(ANSWER));
  });

  test('resolves API response data verbatim', () => {
    const expected = {
      status_code: STATUS_OK,
      result: {
        instance: {
          pager: {},
          rows: [
            {
              instance: ANSWER,
            },
          ],
        },
      },
    };

    expect.assertions(ONE);

    return request('GET', RESOURCE_ID)
      .then(response =>
        expect(response)
          .toEqual(expected));
  });

  test('supports POST', () => {
    expect(isPromise(request('POST', RESOURCE_ID)))
      .toBe(true);
  });

  test('throws if the HTTP method is not supported', () => {
    expect(() => request('FUBAR', RESOURCE_ID))
      .toThrow();
  });

});
