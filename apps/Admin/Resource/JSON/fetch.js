/**
 * Generic client module for Zaaksysteem API transactions.
 * There MUST NOT be knowledge of the Zaaksysteem client *application* here.
 * It SHOULD NOT be used in the client *application* directly.
 */
import dictionary from '../../../library/dictionary';

const { assign, keys } = Object;

const baseHeaders = dictionary({
  Accept: 'application/json',
});

const bodyHeaders = dictionary(assign({}, baseHeaders, {
  'Content-Type': 'application/json',
}));

const getBaseConfig = method => dictionary({
  method,
  headers: baseHeaders,
  credentials: 'same-origin',
});

const getBodyConfig = (method, body) => dictionary({
  method,
  headers: bodyHeaders,
  credentials: 'same-origin',
  body: JSON.stringify(body),
});

const headerFactories = dictionary({
  GET: getBaseConfig,
  POST: getBodyConfig,
});

/**
 * @type {Array}
 */
export const methods = keys(headerFactories);

/**
 * @param {string} method
 * @param {array|object} body
 */
export function getRequestInit(method, body) {
  const get = headerFactories[method];

  if (get) {
    return get(method, body);
  }

  throw new Error(`Method '${method}' is not implemented.`);
}

/**
 * Generic HTTP request function for the Zaaksysteem JSON API.
 * Supports GET and POST.
 *
 * @example
 * json('POST', '/api/order', { id: 42 });
 *
 * @param {string} method The request method.
 * @param {string} url The request URL.
 * @param {Array|Object} [body] The request body.
 * @return {Promise}
 */
export const request = (method, url, body) => window
  .fetch(url, getRequestInit(method, body))
  .then(response => response.json());
