import React from 'react';
import { Action, Body2, Icon } from '@mintlab/ui';
import styleSheet from './DropDownLink.css';

/**
 * @param {Object} props
 * @param {Function} props.action
 * @param {*} props.children
 * @return {ReactElement}
 */
export const DropDownLink = ({
  action,
  children,
}) => (
  <Action
    className={styleSheet.dropdownLink}
    onClick={action}
  >
    <Body2>{children}</Body2>
    <Icon>arrow_drop_down</Icon>
  </Action>
);
