import React from 'react';
import { Snackbar, Render } from '@mintlab/ui';
import get from '../../../../library/get';

/**
 * @param {Object} props
 * @param {Object} props.snackbar
 *   Snackbar configuration data
 * @param {Function} props.t
 *   `i18next` translation function
 * @param {Function} props.onQueueEmpty
 *   Action to dispatch when the Snackbar has finished its
 *   close transition, and there are no messages in the queue
 * @return {ReactElement}
 */
const SnackbarWrapper = ({
  snackbar,
  t,
  onQueueEmpty,
}) => {
  const message = get(snackbar, 'message');

  return (
    <Render condition={message}>
      <Snackbar 
        message={t(message)}
        onQueueEmpty={onQueueEmpty}
      />
    </Render>
  );
};
export default SnackbarWrapper;
