import { createElement } from 'react';
import { Layout } from '@mintlab/ui';
import View from './View';
import Identity from '../Identity';
import { logout } from '../../library/auth';
import { getSegment } from '../../../../library/url';
import { extract } from '../../../../library/object';

/**
 * @param {Array} data
 * @param {string} url
 * @return {number}
 */
const getNavigationIndex = (data, url) => {
  const segment = getSegment(url);
  const index = data
    .map(item => getSegment(item.path))
    .indexOf(segment);

  return index;
};

/**
 * @param {Array} input
 * @param {Function} route
 * @return {Array}
 */
const mapToDrawer = (input, route) =>
  input
    .map(({ icon, label, path }) => ({
      action() {
        route({
          path,
        });
      },
      icon,
      label,
    }));

/**
 * Wrap the {@link View} in the `@mintlab/ui` Layout.
 *
 * @param {Object} props
 * @return {ReactElement}
 */
export default function AdminLayout(props) {
  const [
    { data },
    isDrawerOpen,
    toggleDrawer,
    iframeProps,
  ] = extract('navigation', 'isDrawerOpen', 'toggleDrawer', props);
  const {
    requestUrl,
    route,
  } = iframeProps;

  const CurrentView = createElement(View, iframeProps);

  return createElement(Layout, {
    active: getNavigationIndex(data, requestUrl),
    appBar: createElement(Identity),
    drawer: mapToDrawer(data, route),
    isDrawerOpen,
    onUserClick: logout,
    toggleDrawer,
  }, CurrentView);
}
