import { createElement } from 'react';
import ErrorNotFound from '../ErrorNotFound';
import getComponent from '../../library/router/getComponent';

const IFRAME_OVERLAY_CLASS = 'iframe-overlay';

function setLegacyBodyClass(hasOverlay) {
  if (hasOverlay) {
    document.body.classList.add(IFRAME_OVERLAY_CLASS);
  } else {
    document.body.classList.remove(IFRAME_OVERLAY_CLASS);
  }
}

/**
 * The `View` component is consumed by {@link AdminLayout}
 * and overloaded with one of
 *
 * - {@link ErrorNotFound} (no route found)
 * - {@link InlineFrameLoader} (legacy server-side content)
 * - Original React implementation
 *
 * @param {Object} props
 * @param {boolean} props.hasIframeOverlay
 * @param {boolean} props.isIframeLoading
 * @param {Function} props.onIframeClose
 * @param {Function} props.onIframeOpen
 * @param {Function} props.onIframeLoad
 * @param {Function} props.onIframeUnload
 * @param {string} props.requestUrl
 * @param {Function} props.route
 * @return {ReactElement}
 */
export default function View({
  hasIframeOverlay,
  isIframeLoading,
  onIframeClose,
  onIframeOpen,
  onIframeLoad,
  onIframeUnload,
  requestUrl,
  route,
}) {
  const resolvedRoute = getComponent(requestUrl);

  if (!resolvedRoute) {
    return createElement(ErrorNotFound);
  }

  const [component, data] = resolvedRoute;

  if (typeof data === 'string') {
    const inlineFrameLoader = createElement(component, {
      loading: isIframeLoading,
      onOverlayClose: onIframeClose,
      onOverlayOpen: onIframeOpen,
      onLoad: onIframeLoad,
      onUnload: onIframeUnload,
      route,
      url: requestUrl,
    });

    setLegacyBodyClass(hasIframeOverlay);

    return inlineFrameLoader;
  }

  const routeComponent = createElement(component, {
    route,
    segments: data,
  });

  return routeComponent;
}
