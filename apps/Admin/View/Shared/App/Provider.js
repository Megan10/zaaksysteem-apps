/*
 * App entry component.
 */
import React from 'react';
import { Provider as StoreProvider } from 'react-redux';
import { I18nextProvider as I18nProvider } from 'react-i18next';
import { MaterialUiThemeProvider } from '@mintlab/ui';
import Login from './Login';
import Resource from '../ResourceContainer';
import { createStore } from '../../../Store';
import action from '../../../Store/Action';
import { onPopState } from '../../../../library/dom/history';
import i18next from '../../../../library/i18next';
import initialRouteState from '../../library/router/initialState';
import { getUrl } from '../../../../library/url';

const { assign } = Object;
const {
  route: {
    resolve,
  },
} = action;
const initialState = assign({}, initialRouteState);
const store = createStore(initialState);

function dispatchRoute() {
  store.dispatch(resolve({
    path: getUrl(),
  }));
}

onPopState(dispatchRoute);

const Provider = () => (
  <I18nProvider i18n={i18next}>
    <StoreProvider store={store}>
      <MaterialUiThemeProvider>
        <Resource id={['session', 'navigation']}>
          <Login/>
        </Resource>
      </MaterialUiThemeProvider>
    </StoreProvider>
  </I18nProvider>
);

export default Provider;
