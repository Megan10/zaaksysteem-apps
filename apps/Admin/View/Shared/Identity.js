import React from 'react';
import { Body1 } from '@mintlab/ui';
import { translate } from 'react-i18next';
import { DropDownLink } from './DropDownLink';
import { navigate } from '../../../library/url';
import i18n from '../../../library/i18next';
import styleSheet from './Identity.css';

function switchContext() {
  navigate('/intern/');
}

/**
 * Logotype/navigation component.
 *
 * @param {Function} t
 * @return {ReactElement}
 */
export const Identity = ({ t }) => (
  <div
    className={styleSheet.identity}
  >
    <div
      className={styleSheet.logoType}
    >
      <Body1>
        {i18n.t('common:title')}
      </Body1>
    </div>
    <DropDownLink
      action={switchContext}
    >{t('common:admin')}</DropDownLink>
  </div>
);

export default translate()(Identity);
