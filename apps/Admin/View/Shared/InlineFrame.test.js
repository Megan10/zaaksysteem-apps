import InlineFrame from './InlineFrame';

/**
 * @test {InlineFrame}
 */
describe('The `InlineFrame` component', () => {
  describe('has a `getBasePath` instance method', () => {
    test('that gets the first path component segment including leading and trailing slash', () => {
      window.location.assign('/foo/bar');

      const iframe = new InlineFrame({});
      const actual = iframe.getBasePath();
      const expected = '/foo/';

      expect(actual).toBe(expected);
    });
  });
});
