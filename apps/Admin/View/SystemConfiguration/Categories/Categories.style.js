const GREY_VALUE = 50;
const BORDER_RADIUS_MULTIPLIER = 3;

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const categoriesStyleSheet = ({
  palette: {
    common,
    primary,
    grey,
  },
  shape: {
    borderRadius,
  },
  typography,
}) => {
  const activeButton = {
    color: common.white,
    backgroundColor: primary.main,
    '& aside': {
      color: common.white,
    },
  };

  return {
    categories: {
      backgroundColor: grey[GREY_VALUE],
      listStyleType: 'none',
      padding: '0px 30px 10px 24px',
      margin: '0px',
    },
    shadow: {
      boxShadow: 'none',
    },
    regularButton: {
      height: '42px',
      borderRadius: borderRadius * BORDER_RADIUS_MULTIPLIER,
      marginBottom: '4px',
      '& aside': {
        fontWeight: typography.fontWeightRegular,
      },
      '&:hover': activeButton,
    },
    activeButton,
  };
};

