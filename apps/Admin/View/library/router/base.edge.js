/*
 * Edge configuration for base segment routing, extends stable.
 */
import Configuration from '../../SystemConfiguration/SystemConfigurationWrapper';
import stable from './base.iframe';

const { assign } = Object;

const edge = {
  configuratie: Configuration,
};

export default assign(stable, edge);
