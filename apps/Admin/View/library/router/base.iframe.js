/**
 * iFrame base segment dictionary.
 *
 * @type {Object}
 */
const dictionary = {
  catalogus: '/beheer/bibliotheek',
  gebruikers: '/medewerker',
  logboek: '/beheer/logging',
  transactieoverzicht: '/beheer/sysin/transactions',
  koppelingsconfiguratie: '/beheer/sysin/overview',
  gegevensmagazijn: '/beheer/object/datastore',
  configuratie: '/beheer/configuration',
  import: '/beheer/import',
  zaaktypen: '/beheer/zaaktypen',
};

export default dictionary;
