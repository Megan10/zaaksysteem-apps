import { InlineFrameLoader } from '../../Shared/InlineFrameLoader';
import urlMap from './base';

/**
 * @param {string} url
 * @return {Array}
 */
export default function getComponent(url) {
  const [pathComponent] = url.split('?');
  const [, , segment, ...rest] = pathComponent.split('/');

  if (!urlMap.hasOwnProperty(segment)) {
    return null;
  }

  const value = urlMap[segment];

  // iFrame with legacy src
  if (typeof value === 'string') {
    return [InlineFrameLoader, value];
  }

  // Original React component
  return [value, rest];
}
