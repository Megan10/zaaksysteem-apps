import { replaceState } from '../../../../library/dom/history';
import { getSegment, getUrl } from '../../../../library/url';
import urlMap from './base';

const { keys } = Object;
const HIERARCHY = '/';

/**
 * @ignore
 * @type {string}
 */
export const [DEFAULT_SEGMENT] = keys(urlMap);

/**
 * @ignore
 * @param {string} requestUrl
 * @return {string}
 */
export function getDefaultRoute(requestUrl) {
  const joinToken = requestUrl.endsWith(HIERARCHY) ?
    '' :
    HIERARCHY;
  const replacedUrl = [requestUrl, DEFAULT_SEGMENT].join(joinToken);

  return replacedUrl;
}

/**
 * @ignore
 * @param {Function} callSideEffect
 * @return {string}
 */
export function normalizeRequestUrl(callSideEffect) {
  const requestUrl = getUrl();
  const segment = getSegment(requestUrl);

  if (segment) {
    return requestUrl;
  }

  const defaultRoute = getDefaultRoute(requestUrl);

  callSideEffect(defaultRoute);

  return defaultRoute;
}

export default {
  route: normalizeRequestUrl(replaceState),
};
