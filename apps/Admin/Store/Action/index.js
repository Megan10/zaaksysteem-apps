import actionMan from '../../../library/redux/actionMan';

/**
 * A simplified representation of the store actions.
 * The default export of this module transforms the elements of the
 * array leaves in the tree to action creators that return an object
 * with the signature
 *
 * - `{string}` type
 * - `{*}` payload
 *
 * An action creator yields its associated action type when coerced
 * to a string.
 *
 * @type {Object}}
 */
export const action = {
  form: [
    'set',
  ],
  resource: [
    'request',
    'respond',
  ],
  route: [
    'invoke',
    'resolve',
  ],
  ui: {
    dialog: [
      'show',
      'hide',
    ],
    drawer: [
      'open',
      'close',
    ],
    iframe: {
      overlay: [
        'open',
        'close',
      ],
      window: [
        'load',
        'unload',
      ],
    },
    snackbar: [
      'show',
      'clear',
    ],
  },
};

export default actionMan(action);
