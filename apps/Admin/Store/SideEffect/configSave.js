import action from '../Action';
import get from '../../../library/get';

const { keys } = Object;
const {
  resource: {
    respond,
  },
} = action;

/**
 * Update an item values after saving.
 */
/* eslint complexity: [2, 10] */
export default {
  [respond](data, store) {
    const NAME = 'config:save';
    const STATUS_OK = 200;
    const state = store.getState();
    const part = keys(data).find(entry => entry === NAME);
    const payload = data[part];
    const { dispatch } = store;
    const status = get(payload, '$set.status');

    if (!payload || status !== STATUS_OK) {
      return Promise.resolve();
    }

    const { $set: { data: updatedItems }} = payload;
    const {
      form,
      resource: {config: {data: {items }}},
    } = state;

    const changes = updatedItems.reduce((accumulator, item) => {
      const index = items.findIndex(existing => existing.reference === item.reference);
      const value = form[item.name];

      if (!index) {
        return accumulator;
      }

      accumulator[index] = {
        value: {
          $set: value,
        },
      };
      
      return accumulator;
    }, {});

    if (keys(changes).length) {
      dispatch(respond({ config: { data: { items: changes }}} ));
    }

    return Promise.resolve();
  },
};
