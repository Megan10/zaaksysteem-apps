import action from '../Action';
import resource from '../../Resource';
import get from '../../../library/get';

const { assign } = Object;
const {
  resource: {
    request,
    respond,
  },
} = action;

function normalize(parameters) {

  const idSet = parameters
    .map(orderedPairTuple => {
      const [identifier] = orderedPairTuple;
      return (identifier.hasOwnProperty('id')) ? identifier.id : identifier;
    });

  const valuePromises = parameters
    .map(orderedPairTuple =>
      resource(...orderedPairTuple));

  return [idSet, valuePromises];
}

export default {
  [request](parameters) {
    const [idSet, promises] = normalize(parameters);

    const reduceIdValueDictionary = (accumulator, value, index) =>
      assign(accumulator, {
        [idSet[index]]: {$set: value},
      });

    const createResponseAction = response => {
      const data = response.reduce(reduceIdValueDictionary, {});
      const [[part]] = parameters;
      const meta = get(part, 'meta');
      return respond(data, null, meta);
    };

    return Promise
      .all(promises)
      .then(response =>
        () => createResponseAction(response));
  },
};
