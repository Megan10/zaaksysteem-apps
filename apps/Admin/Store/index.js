import { createReduxStore } from '../../library/redux/createStore';
import { formReducer as form } from './Reducer/form';
import { resourceReducer as resource } from './Reducer/resource';
import { routeReducer as route } from './Reducer/route';
import { uiReducer as ui } from './Reducer/ui';
import middlewares from './Middleware';
import sideEffects from './SideEffect';

const reducers = {
  form,
  resource,
  route,
  ui,
};

/**
 * Create a store with initial state. Since the initial state
 * can be impure, this is a factory function that is called
 * from the application.
 *
 * @param {Object} initialState
 *   The initial state of the store.
 * @return {Store}
 */
export const createStore = initialState =>
  createReduxStore(reducers, initialState, middlewares, sideEffects);
