# Store

> [Redux](https://redux.js.org/) state container.

## *Store* directory structure

### Action

Documentation: 
[Actions](https://redux.js.org/docs/basics/Actions.html)

### Middleware

Documentation:
[Middleware](https://redux.js.org/advanced/middleware)

### Reducer

Documentation: 
[Reducers](https://redux.js.org/docs/basics/Reducers.html)

### SideEffect

[Middleware](https://redux.js.org/docs/advanced/Middleware.html) 
factory configuration for 
[Async Actions](https://redux.js.org/docs/advanced/AsyncActions.html).
