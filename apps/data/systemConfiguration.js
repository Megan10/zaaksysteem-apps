import i18n from '../library/i18next';

/**
 * @type {Array}
 */
const map = [
  {
    name: i18n.t('systemConfiguration:categories:cases'),
    slug: 'cases',
    icon: 'folder_shared',
    fieldSets: [
      {
        title: i18n.t('systemConfiguration:emailTemplate'),
        description: i18n.t('systemConfiguration:emailTemplate'),
        fields: ['allocation_notification_template_id'],
      },
      {
        title: i18n.t('systemConfiguration:rejectedCases'),
        description: i18n.t('systemConfiguration:rejectedCases'),
        fields: ['case_distributor_group', 'case_distributor_role'],
      },
    ],
  },
  {
    name: i18n.t('systemConfiguration:categories:users'),
    slug: 'users',
    icon: 'supervised_user_circle',
    fieldSets: [
      {
        title: i18n.t('systemConfiguration:newUsers'),
        description: i18n.t('systemConfiguration:newUsers'),
        fields: ['first_login_confirmation', 'first_login_intro', 'new_user_template'],
      },
      {
        title: i18n.t('systemConfiguration:dashboardNotEditable'),
        description: i18n.t('systemConfiguration:dashboardNotEditable'),
        fields: ['disable_dashboard_customization'],
      },
      {
        title: i18n.t('systemConfiguration:signatureUploadRole'),
        description: i18n.t('systemConfiguration:signatureUploadRole'),
        fields: ['signature_upload_role'],
      },
    ],
  },
  {
    name: i18n.t('systemConfiguration:categories:pip'),
    slug: 'pip',
    icon: 'web',
    fieldSets: [
      {
        title: i18n.t('systemConfiguration:pipFeedback'),
        description: i18n.t('systemConfiguration:pipFeedback'),
        fields: ['feedback_email_template_id'],
      },
      {
        title: i18n.t('systemConfiguration:pipIntro'),
        description: i18n.t('systemConfiguration:pipIntro'),
        fields: ['pip_login_intro'],
      },
    ],
  },
  {
    name: i18n.t('systemConfiguration:categories:documents'),
    slug: 'documents',
    icon: 'insert_drive_file',
    fieldSets: [
      {
        title: i18n.t('systemConfiguration:documentIntake'),
        description: i18n.t('systemConfiguration:documentIntake'),
        fields: ['file_username_seperator', 'document_intake_user'],
      },
      {
        title: i18n.t('systemConfiguration:jodConverter'),
        description: i18n.t('systemConfiguration:jodConverter'),
        fields: ['jodconverter_url'],
      },
    ],
  },
  {
    name: i18n.t('systemConfiguration:categories:about'),
    slug: 'about',
    icon: 'fingerprint',
    fieldSets: [
      {
        title: i18n.t('systemConfiguration:customerInfo'),
        description: i18n.t('systemConfiguration:customerInfo'),
        fields: ['customer_info_website', 'customer_info_naam', 'customer_info_naam_lang', 'customer_info_naam_kort'],
      },
      {
        title: i18n.t('systemConfiguration:customerAddress'),
        description: i18n.t('systemConfiguration:customerAddress'),
        fields: ['customer_info_straatnaam', 'customer_info_postcode', 'customer_info_woonplaats'],
      },
      {
        title: i18n.t('systemConfiguration:customerExtra'),
        description: i18n.t('systemConfiguration:customerExtra'),
        fields: ['customer_info_postbus', 'customer_info_postbus_postcode'],
      },
      {
        title: i18n.t('systemConfiguration:customerPhone'),
        description: i18n.t('systemConfiguration:customerPhone'),
        fields: ['customer_info_telefoonnummer', 'customer_info_faxnummer'],
      },
      {
        title: i18n.t('systemConfiguration:customerMail'),
        description: i18n.t('systemConfiguration:customerMail'),
        fields: ['customer_info_zaak_email', 'customer_info_email'],
      },
    ],
  },
  {
    name: i18n.t('systemConfiguration:categories:premium'),
    slug: 'premium',
    icon: 'star',
    fieldSets: [
      {
        title: i18n.t('systemConfiguration:premium'),
        description: i18n.t('systemConfiguration:premium'),
        fields: ['mor_app_todo', 'bbv_app_todo', 'files_locally_editable'],
      },
    ],
  },
  {
    name: i18n.t('systemConfiguration:categories:other'),
    slug: 'other',
    icon: 'card_giftcard',
    fieldSets: [
      {
        title: i18n.t('systemConfiguration:bagPriority'),
        description: i18n.t('systemConfiguration:bagPriority'),
        fields: ['bag_priority_gemeentes'],
      },
      {
        title: i18n.t('systemConfiguration:publicManpage'),
        description: i18n.t('systemConfiguration:publicManpage'),
        fields: ['public_manpage'],
      },
      {
        title: i18n.t('systemConfiguration:customRelationRoles'),
        description: i18n.t('systemConfiguration:customRelationRoles'),
        fields: ['custom_relation_roles'],
      },
      {
        title: i18n.t('systemConfiguration:location'),
        description: i18n.t('systemConfiguration:location'),
        fields: ['customer_info_latitude', 'customer_info_longitude'],
      },
      {
        title: i18n.t('systemConfiguration:pipAuthorization'),
        description: i18n.t('systemConfiguration:pipAuthorization'),
        fields: ['subject_pip_authorization_confirmation_template_id'],
      },
    ],
  },
  {
    name: i18n.t('systemConfiguration:categories:deprecated'),
    slug: 'deprecated',
    icon: 'save',
    fieldSets: [
      {
        title: i18n.t('systemConfiguration:externalSearch'),
        description: i18n.t('systemConfiguration:externalSearch'),
        fields: ['requestor_search_extension_active', 'requestor_search_extension_name', 'requestor_search_extension_href'],
      },
      {
        title: i18n.t('systemConfiguration:changePassword'),
        description: i18n.t('systemConfiguration:changePassword'),
        fields: ['users_can_change_password'],
      },
      {
        title: i18n.t('systemConfiguration:stufZkn'),
        description: i18n.t('systemConfiguration:stufZkn'),
        fields: ['enable_stufzkn_simulator'],
      },
      {
        title: i18n.t('systemConfiguration:pdfAnnotations'),
        description: i18n.t('systemConfiguration:pdfAnnotations'),
        fields: ['pdf_annotations_public'],
      },
      {
        title: i18n.t('systemConfiguration:wordApp'),
        description: i18n.t('systemConfiguration:wordApp'),
        fields: ['wordapp'],
      },
      {
        title: i18n.t('systemConfiguration:allowedTemplates'),
        description: i18n.t('systemConfiguration:allowedTemplates'),
        fields: ['allowed_templates'],
      },
    ],
  },
];

export default map;
