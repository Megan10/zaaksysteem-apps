const whitelist = require('./whitelist');
const { keys } = Object;

const isValidPackage = (context, name) =>
  keys(whitelist[context])
    .includes(name);

module.exports = isValidPackage;
