const { join } = require('path');
const { cyan, green, yellow } = require('chalk');
const { prompt } = require('inquirer');
const { install, uninstall } = require('./npm');
const { error, info } = require('./methods');
const isValidPackage = require('./isValidPackage');
const exit = require('./exit');
const exec = require('./exec');
const { read, write } = require('./file');
const {
  CONTAINER_ROOT,
  NODE_ROOT,
  VENDOR_PACKAGE_PATH,

} = require('./constants');

const { keys } = Object;
const installationContexts = ['build', 'vendor'];
const SPLICE_LENGTH = 1;
const VENDOR_MANIFEST_PATH = `${NODE_ROOT}/vendor/package.json`;

/**
 * Get the parsed vendor manifest file content.
 * You **cannot** use `require` here because modules are cached.
 *
 * @return {Object}
 */
const getVendorManifest = () =>
  read(VENDOR_MANIFEST_PATH);

/**
 * @param {Object} data
 */
function setVendorManifest(data) {
  write(VENDOR_MANIFEST_PATH, data);
}

/**
 * @param {string} context
 * @return {boolean}
 */
const isValidContext = context =>
  installationContexts.includes(context);

function rebuild() {
  exec('./bin/vendor');
}

/**
 * Dictionary of spoofable packages in the `@mintlab` scope.
 * The first property becomes the default choice of the prompt.
 *
 * @type {Object}
 */
const spoofablePackages = {
  /**
   * @return {Promise}
   */
  '@mintlab/ui': function mintlabUi() {
    const cwd = join(CONTAINER_ROOT, 'ui');
    const target = join(VENDOR_PACKAGE_PATH, 'node_modules', '@mintlab', 'ui');

    info('Copying distribution artefacts.');

    return exec('cp', [
      '-rf',
      'distribution',
      target,
    ], cwd);
  },
};

/**
 * @param {string} packageName
 */
function runSpoof(packageName) {
  spoofablePackages[packageName]()
    .then(() =>
      exec('./bin/vendor'));
}

function spoofPrompt() {
  const choices = keys(spoofablePackages);
  const [defaultChoice] = choices;
  const questions = [
    {
      choices,
      default: defaultChoice,
      message: 'Choose a spoofable package:',
      name: 'packageName',
      type: 'list',
    },
  ];

  /**
   * @param {Object} answers
   * @param answers.packageName
   */
  function use({ packageName }) {
    runSpoof(packageName);
  }

  prompt(questions)
    .then(use);
}

/**
 * Replace an installed package in the `@mintlab` scope with
 * the distribution files of a mounted volume under
 * `/opt/zaaksysteem-apps/@mintlab`.
 * (NB: symbolic links appear not to be supported by the
 * webpack `file-loader` plugin.)
 *
 * @param {string} packageName
 */
function spoof(packageName) {
  if (!packageName) {
    info('No package name received.');
    spoofPrompt();
    return;
  }

  if (!spoofablePackages.hasOwnProperty(packageName)) {
    info(`Package ${green(packageName)} cannot be spoofed.`);
    spoofPrompt();
    return;
  }

  runSpoof(packageName);
}

/**
 * @param {Function} command
 * @param {string} context
 */
function packagePrompt(command, context) {
  const questions = [
    {
      message: 'Provide a package name:',
      name: 'packageName',
      type: 'input',
    },
  ];

  /**
   * @param {Object} answers
   * @param {string} answers.packageName
   */
  function use({ packageName }) {
    command(context, packageName);
  }

  prompt(questions)
    .then(use);
}

function contextPrompt(command) {
  const questions = [
    {
      choices: [
        {
          name: 'build: Node.js runtime environment packages',
          value: 'build',
        },
        {
          name: 'vendor: client-side packages',
          value: 'vendor',
        },
      ],
      message: 'Choose an installation context:',
      name: 'context',
      type: 'list',
    },
  ];

  function use({ context }) {
    command(context);
  }

  prompt(questions)
    .then(use);
}

/**
 * @param {string} context
 * @param {string} packageName
 */
function addVendorPrompt(context, packageName) {
  const vendorManifest = getVendorManifest();
  const { dll } = vendorManifest;
  const choices = keys(dll);
  const questions = [
    {
      choices,
      message: `Choose one or more stacks to add ${cyan(packageName)}:`,
      name: 'presets',
      type: 'checkbox',
    },
  ];

  function use({ presets }) {
    if (!presets.length) {
      exit('no stack received');
    }

    info(`adding ${cyan(packageName)} to ${yellow(context)} context`);
    install(context, packageName)
      .then(function postInstall() {
        const vendorManifest = getVendorManifest();

        for (const stack of presets) {
          if (!vendorManifest.dll[stack].includes(packageName)) {
            vendorManifest.dll[stack].push(packageName);
            vendorManifest.dll[stack].sort();
          }
        }

        setVendorManifest(vendorManifest);
        rebuild();
      })
      .catch(function catchRejection(reason) {
        error(reason);
      });
  }

  prompt(questions)
    .then(use);
}

function removeVendorPrompt(choices, callback) {
  const questions = [
    {
      choices,
      message: 'Choose the stacks to remove',
      name: 'stacks',
      type: 'checkbox',
    },
  ];

  /**
   * @param {Object} answers
   * @param {Array} answers.stacks
   */
  function use({ stacks }) {
    callback(stacks);
  }

  prompt(questions)
    .then(use);
}

/**
 * @param {string} context
 * @param {string} packageName
 */
function removeVendor(context, packageName) {
  const vendorManifest = getVendorManifest();
  const { dll } = vendorManifest;

  const filterStacks = key =>
    dll[key]
      .includes(packageName);

  const choices = keys(dll)
    .filter(filterStacks);

  /**
   * Remove the package name from the provided dll entries in the
   * vendor package manifest and uninstall the package if no entries
   * are left.
   *
   * @param {Array} stacks
   */
  function task(stacks) {
    const vendorManifest = getVendorManifest();
    const { dll } = vendorManifest;

    for (const key of stacks) {
      const value = dll[key];
      const index = value.indexOf(packageName);

      value.splice(index, SPLICE_LENGTH);
    }

    setVendorManifest(vendorManifest);

    if (choices.length === stacks.length) {
      uninstall(context, packageName)
        .then(rebuild);
    }
  }

  /**
   * If there is only one stack, skip the prompt.
   */
  function updateStack() {
    if (choices.length === SPLICE_LENGTH) {
      task(choices);
    } else {
      removeVendorPrompt(choices, task);
    }
  }

  if (choices.length) {
    updateStack();
  } else {
    error(`package ${green(packageName)} not found in any stack`);
    info('running uninstall anyway to clean up');
    uninstall(context, packageName)
      .then(rebuild);
  }
}

/**
 * Automatically prompt for missing argument values.
 *
 * @param {string} context
 * @param {string} packageName
 * @param {Function} command
 * @return {boolean}
 */
function autoPrompt(context, packageName, command) {
  if (!isValidContext(context)) {
    info(`no valid ${yellow('context')} received`);
    contextPrompt(command);

    return true;
  }

  if (!packageName) {
    info(`no valid ${green('package name')} received`);
    packagePrompt(command, context);

    return true;
  }

  return false;
}

/**
 * @param {string} string
 * @param {string} packageName
 */
function add(context, packageName) {
  if (!autoPrompt(context, packageName, add)) {
    if (!isValidPackage(context, packageName)) {
      exit(`the package ${green(packageName)} is not whitelisted`);
    }

    const map = {
      build() {
        info(`adding ${cyan(packageName)} to ${yellow(context)} context`);
        install(context, packageName)
          .catch(function catchRejection(reason) {
            error(reason);
          });
      },
      vendor() {
        addVendorPrompt(context, packageName);
      },
    };

    map[context]();
  }
}

/**
 * @param {string} context
 * @param {string} packageName
 */
function remove(context, packageName) {
  if (!autoPrompt(context, packageName, remove)) {
    info(`removing ${cyan(packageName)} from ${yellow(context)} context`);

    const map = {
      build() {
        uninstall(context, packageName)
          .catch(function catchRejection(reason) {
            error(reason);
          });
      },
      vendor() {
        removeVendor(context, packageName);
      },
    };

    map[context]();
  }
}

module.exports = {
  add,
  remove,
  spoof,

  start() {
    exec('ln', [
      '-sf',
      './src/Admin/legacy.css ../root/admin/',
      '../root/admin/',
    ]);
    require('./develop');
  },

  build() {
    process.env.NODE_ENV = 'production';
    exec('./bin/build');
    exec('cp', [
      './src/Admin/legacy.css',
      '../root/admin/',
    ]);
  },

  lint(...rest) {
    exec('npx', [
      'eslint',
      './bin',
      './src',
      ...rest,
    ]);
  },

  rtfm() {
    exec('npx', [
      'esdoc',
      '-c', './src/.esdoc',
    ])
      .then(() => exec('npx', [
        'http-server',
      ]));
  },

  test(...rest) {
    exec('npx', [
      'jest',
      '--config=./src/test/jest.config.js',
      ...rest,
    ]);
  },
};
