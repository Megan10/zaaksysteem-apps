// Enable swapping of sub-apps under /admin/* with the environment variable
// WEBPACK_BUILD_TARGET=edge

const { NormalModuleReplacementPlugin } = require('webpack');

const replacements = [
  new NormalModuleReplacementPlugin(
    /\/library\/router\/base.js$/,
    'base.edge.js'
  ),
  new NormalModuleReplacementPlugin(
    /\/Store\/Middleware\/route.js$/,
    'route.edge.js'
  ),
];

/**
 * @param {Object} configuration
 * @param {Array} configuration.plugins
 */
function edge({ plugins }) {
  plugins.push(...replacements);
}

module.exports = edge;
