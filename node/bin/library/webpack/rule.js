const { assign } = Object;
const exclude = /\/node_modules\//;

function getExtensionExpression(data) {
  if (typeof data === 'string') {
    return data;
  }

  return `(?:${data.join('|')})`;
}

/**
 * @private
 * @param {string} extension
 * @param {Array} use
 * @param {Object} options
 * @return {Object}
 */
const ruleFactory = (extension, use, options) =>
  assign(options, {
    test: new RegExp(`\\.${getExtensionExpression(extension)}$`),
    use,
  });

/**
 * Package rules **include** `node_modules`
 *
 * @param extension
 * @param use
 * @return {Object}
 */
const packageRule = (extension, use) =>
  ruleFactory(extension, use, {
    include: exclude,
  });

/**
 * App rules **exclude** `node_modules`.
 *
 * @param {string} extension
 * @param {Array} use
 * @return {Object}
 */
const rule = (extension, use) =>
  ruleFactory(extension, use, {
    exclude,
  });

assign(exports, {
  packageRule,
  rule,
});
