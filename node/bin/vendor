#! /usr/bin/env node
/***********************************************************************
 * Build the vendor bundles. Current scope:
 * - React universe
 * - Mintlab UI
 ***********************************************************************/
const { copyFileSync } = require('fs');
const { join } = require('path');
const webpack = require('webpack');
const { read } = require('./library/file');
const { info } = require('./library/methods');
const {
  ENVIRONMENT,
  SERVER_DOCUMENT_ROOT,
  VENDOR_PACKAGE_PATH,
  VENDOR_SERVER_PATH,
} = require('./library/constants');
const {
  onWebpackComplete,
} = require('./library/methods');
const {
  dllPluginFactory,
  dllReferencePluginFactory,
  library,
} = require('./library/webpack/dll');

const { dll } = read(`${VENDOR_PACKAGE_PATH}/package.json`);

/**
 * React DLL bundle dependencies.
 *
 * @type {Array}
 */
const react = dll['react:redux'];

const scope = '@mintlab';
const packageName = 'ui';

/**
 * Mintlab DLL bundle dependencies.
 *
 * @type {Array}
 */
const ui = [
  `${scope}/${packageName}`,
];

/**
 * Webpack output filename pattern.
 * ZS-TODO: use hash for filename
 *
 * @type {string}
 */
const filename = '[name].js';

/**
 * @param {Object} entry
 * @param {Object} plugins
 * @return {Object}
 */
const configurationFactory = ({ entry, plugins }) => ({
  entry,
  output: {
    filename,
    library,
    path: VENDOR_SERVER_PATH,
  },
  mode: ENVIRONMENT,
  plugins,
  resolve: {
    modules: [
      join(VENDOR_PACKAGE_PATH, 'node_modules'),
    ],
  },
});

const reactConfiguration = configurationFactory({
  entry: {
    react,
  },
  plugins: [
    dllPluginFactory(),
  ],
});

/**
 * The DLL bundle build loses the `import()` chunks.
 */
function copyOrphanChunks() {
  const queue = [
    'ui.select.js',
    'ui.wysiwyg.js',
  ]
    .map(baseName => [
      join(VENDOR_PACKAGE_PATH, 'node_modules', scope, packageName, 'distribution', baseName),
      join(VENDOR_SERVER_PATH, baseName),
    ]);

  for (const [from, to] of queue) {
    copyFileSync(from, to);
  }
}

/**
 * ZS-TODO: sequential build
 * Implement a more elegant way to chain
 * multiple builds that depend on each other.
 *
 * @param {Array} rest
 */
function reactCallback(...rest) {
  onWebpackComplete(...rest);
  copyOrphanChunks();

  const uiConfiguration = configurationFactory({
    entry: {
      ui,
    },
    plugins: [
      dllReferencePluginFactory('react'),
      dllPluginFactory(),
    ],
  });

  webpack(uiConfiguration, onWebpackComplete);
}

info('Building the vendor bundles.');
webpack(reactConfiguration, reactCallback);
