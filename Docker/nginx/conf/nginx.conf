user nobody nogroup;
worker_processes auto;          # auto-detect number of logical CPU cores

events {
    worker_connections 512;       # set the max number of simultaneous connections (per worker process)
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    server_tokens off;

    keepalive_timeout  65;

    limit_conn_zone $host zone=perserver:10m;

    gzip  on;
    gzip_http_version 1.0;
    gzip_comp_level 2;
    gzip_proxied any;
    gzip_vary off;
    gzip_types text/plain text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript application/javascript application/json;
    gzip_min_length  1000;
    gzip_disable     "MSIE [1-6]\.";

    server_names_hash_bucket_size 128;
    types_hash_max_size 2048;
    types_hash_bucket_size 64;

    access_log /dev/stdout;

    # When proxy tells us via "X-Forwarded-Proto: https", set the variable $fe_https to "on"
    # default to off.
    map $http_x_forwarded_proto $fe_https {
      default off;
      https on;
    }
    
    server {
        listen 81 default_server;
        server_name zaaksysteem;        # Mandatory to make limit_conn work...

        root /opt/zaaksysteem-apps/root;

        location ~ ^/(?<app>admin) {
            root /opt/zaaksysteem-apps/root;
            try_files $uri $uri/ /$app/index.html;

            error_page 404 =200 /$app/index.html;
        }


        location ^~ /\.ht {
            deny all;
        }
    }
}
