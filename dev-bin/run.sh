#!/bin/bash

if [ "$@" ]; then
    docker run -ti --rm registry.gitlab.com/zaaksysteem/zaaksysteem-apps:dev $@
else
    docker run -p 8765:80 --rm registry.gitlab.com/zaaksysteem/zaaksysteem-apps:dev
fi
